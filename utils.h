//
// Created by Vanilor on 4/2/2020.
//

#pragma once

#include "consts.h"

short checkProbabilities(const double* array, int alphabetSize);
void fileExport(byte* content, byte* alphabet);
byte* handleInput(byte* alphabet);
void emptyBuffer();
byte* getString(byte* alphabet, int filtered);
short isInAlphabet(byte c, byte* alphabet, int alphabetSize);
unsigned int modulo_u(int value, unsigned int m);
int array_search(byte needle, byte* haystack, int haystackSize);
byte convertExtendedASCII(byte c);
void printHexPool(byte* text, int textSize, byte* title);