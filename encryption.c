//
// Created by Vanilor on 4/2/2020.
//

#include <string.h>
#include <stdio.h>

#include "consts.h"
#include "utils.h"
#include "encryption.h"



byte *vigenereEncrypt(byte *input, int inputSize, byte *alphabet, int alphabetSize, byte *key, int keySize) {
	
	return encryption(input, inputSize, key, keySize, alphabet, alphabetSize, MODE_ENCRYPT);
}

byte *vigenereDecrypt(byte *input, int inputSize, byte *alphabet, int alphabetSize, byte *key, int keySize) {
	
	return encryption(input, inputSize, key, keySize, alphabet, alphabetSize, MODE_DECRYPT);
}

byte *caesarEncrypt(byte *input, int inputSize, byte *alphabet, int alphabetSize, byte key) {
	
	return encryption(input, inputSize, &key, 1, alphabet, alphabetSize, MODE_ENCRYPT);
}


byte *caesarDecrypt(byte *input, int inputSize, byte *alphabet, int alphabetSize, byte key) {
	
	return encryption(input, inputSize, &key, 1, alphabet, alphabetSize, MODE_DECRYPT);
}

byte *encryption(byte *input, int inputSize,
				 byte *key, int keySize,
				 byte *alphabet, int alphabetSize,
				 unsigned int encryptOrDecrypt){
	
	
	int oldKey, keyKey;
	byte* output = calloc(inputSize + 1, sizeof(byte));
	
	for (int i = 0; i < inputSize; i++) {
		
		if(!isInAlphabet(input[i], alphabet, alphabetSize))
			break;
		
		oldKey = array_search(input[i], alphabet, alphabetSize);
		keyKey = array_search(key[i % keySize], alphabet, alphabetSize);
		
		if (oldKey < 0 || keyKey < 0) {
			printf("Cannot find those input or key keys : %d %d in the alphabet.\n input char : %c ; key : %c \n", oldKey, keyKey, input[i], key[i % keySize]);
			continue;
		}
		
		/*
		 * It's optimizable by using a mathematical trick as follow, assuming MODE_ENCRYPT = 1 && MODE_DECRYPT = 0 :
		 *
		 * 	output[i] = alphabet[modulo_u(oldKey + (2*encryptOrDecypt - 1)*keyKey, alphabetSize)];
		 *
		 * 	Then we don't need any if else statement. We keep this only for code clarity and make it easy to read.
		 */
		if (encryptOrDecrypt == MODE_DECRYPT)
			output[i] = alphabet[modulo_u(oldKey - keyKey, alphabetSize)];
		else
			output[i] = alphabet[modulo_u(oldKey + keyKey, alphabetSize)];
	}
	
	output[inputSize] = '\0';
	return output;
	
}

void encryptionMode(byte* alphabet, unsigned int algorithm){
	
	//@TODO Change int to short type
	int encryptMode = MODE_ENCRYPT;
	
	byte* input = handleInput(alphabet);
	
	if(input == NULL)
		encryptionMode(alphabet, algorithm);
	
	printf("Entrez la clé à utiliser : \n");
	byte* key = getString(alphabet, MODE_STRING_FILTERED);
	
	do {
		
		printf("Sélectionnez le mode (1 pour Chiffrement, 0 pour Déchiffrement) : \n");
		scanf("%d", &encryptMode);
		emptyBuffer();
		
	} while (encryptMode != MODE_ENCRYPT && encryptMode != MODE_DECRYPT);
	
	
	if (algorithm == ALGORITHM_CAESAR) {
		
		if (encryptMode == MODE_ENCRYPT)
			input = caesarEncrypt(input, strlen(input), alphabet, strlen(alphabet), key[0]);
		else
			input = caesarDecrypt(input, strlen(input), alphabet, strlen(alphabet), key[0]);
		
	}
		
	//Use the Vigenere algorithm by default
	else {
		
		if(encryptMode == MODE_ENCRYPT)
			input = vigenereEncrypt(input, strlen(input), alphabet, strlen(alphabet), key, strlen(key));
		else
			input = vigenereDecrypt(input, strlen(input), alphabet, strlen(alphabet), key, strlen(key));
		
	}
	
	printf("Résultat : %s \n", input);
	fileExport(input, alphabet);
	
}