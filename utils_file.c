//
// Created by Vanilor on 4/3/2020.
//

#include <stdio.h>

#include "consts.h"
#include "utils.h"

/**
 * ****************************
 * FILE UTILS
 * ****************************
 */

int allocMessage(FILE *f, byte **message, int *size){
	
	fseek(f, 0, SEEK_END);
	*size = ftell(f);
	rewind(f);
	
	*message = calloc(*size, sizeof(byte));
	if(!(*message))
		return 1;
	
	return 0;

}

/**
 *
 * allocMessage() here is unused as we only need a realloc function in order to allocate dynamically the message
 *
 * @param f
 * @return byte* the message retrieved
 *
 */
void encode32(FILE *f, byte *input, int size){
	
	byte nextByte, buffer;
	int fSize = 0;
	
	unsigned int i = 0;
	
	while (!feof(f) || (i < size)) {
		
		fSize = ftell(f);
		
		fread(&nextByte, sizeof(byte), 1, f);
		//Avoiding a strange bug that results to set the f pointer to -1 and read the last char twice
		// @TODO Find the cause of this bug and avoid it in a better way
		if(fSize == ftell(f))
			break;
		
		//Handle extended ASCII symbols (convert accented and lowercase characters into non-accented uppercase corresponding)
		if (nextByte > 0x7F) {
			
			fread(&buffer, sizeof(byte), 1, f);
			
			/*
				@TODO Understand this. Wtf I did ?
				
				Example excepted output for à char : 0x00E0(16) 224(10) (Validated)
				Inserts the buffer byte at the right of the current nextByte in order to handle 2-bytes Extended ASCII characters
			 
				I don't know how I tough to realize that.
			    Please refer to https://www.utf8-chartable.de/ for char encoding set.
			 
				Explanation :
					(~nextByte << 4)    Performs an inversion of nextByte and and right shift of 4 because we want to read from LSB to MSB and set the 4 first bits as the MSBs of result
					| buffer            Performs an OR bitwise with buffer in order to retrieve the UTF-8 index array position
			 */
			nextByte = (~nextByte << 4) | buffer;
			
			nextByte = convertExtendedASCII(nextByte);
		}
		//Convert lowercase characters into uppercase
		else if((nextByte >= 0x61) && (nextByte <= 0x7A))
			nextByte -= 0x20;
		
		
		input[i++] = nextByte;
		/*
		 * Another way to alloc the message dynamically as we run into the file, avoiding to use the allocMessage() function
		 *
		 * input = realloc(input, i + 1);
		 */
	}
	
	//Add null-terminating character
	input[i] = '\0';
	
}

void decode32(File *f, byte *content, int size) {
	
	fprintf(f, "%s", content);
	
}