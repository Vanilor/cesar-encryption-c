//
// Created by Vanilor on 4/2/2020.
//

#pragma once

byte *vigenereEncrypt(byte *input, int inputSize, byte *alphabet, int alphabetSize, byte *key, int keySize);
byte *vigenereDecrypt(byte *input, int inputSize, byte *alphabet, int alphabetSize, byte *key, int keySize);

byte *caesarEncrypt(byte *input, int inputSize, byte *alphabet, int alphabetSize, byte key);
byte *caesarDecrypt(byte *input, int inputSize, byte *alphabet, int alphabetSize, byte key);

byte *encryption(byte *input, int inputSize,
                 byte *key, int keySize,
                 byte *alphabet, int alphabetSize,
                 unsigned int encryptOrDecrypt);

void encryptionMode(byte* alphabet, unsigned int algorithm);