//
// Created by Vanilor on 4/3/2020.
//

#include <string.h>
#include <stdlib.h>

#include "consts.h"
#include "utils.h"
#include "utils_file.h"
#include "cryptanalysis.h"
#include "encryption.h"

void cryptanalysisMode(byte* alphabet, double* theoricalDistrib, double langageICN, unsigned int algorithm){
	
	//@TODO Implement alphabetSize && cipherSize
	byte* cipher = handleInput(alphabet);
	byte* deciphered;
	byte mostProbableKey;
	int mostProbableKeyLenght;
	
	//Loop in cryptanalysis mode in case an input file couldn't be open
	if(cipher == NULL)
		cryptanalysisMode(alphabet, theoricalDistrib, langageICN, algorithm);
	
	if(algorithm == ALGORITHM_CAESAR){
		
		mostProbableKey = alphabet[caesarCryptanalysis(cipher, (int)strlen(cipher), theoricalDistrib, alphabet, (int)strlen(alphabet))];
		deciphered =  caesarDecrypt(cipher, strlen(cipher), alphabet, strlen(alphabet), mostProbableKey);
		
		printf("La clé la plus probable est %c. \n", mostProbableKey);
		printf("Texte déchiffré avec la clé %c : %s \n\n", mostProbableKey, deciphered);
		
	}
	else {
		byte* key;
		vigenereCryptanalysis(cipher, (int)strlen(cipher), theoricalDistrib, alphabet, (int)strlen(alphabet), langageICN, (int)strlen(cipher), &key, &mostProbableKeyLenght);
		deciphered = vigenereDecrypt(cipher, strlen(cipher), alphabet, strlen(alphabet), key, strlen(key));
		
		printf("La clé la plus probable est %s avec une longueur de %d \n", key, mostProbableKeyLenght);
		printf("Le texte déchiffré avec la clé %s est : \n %s \n\n", key, deciphered);
	}
	
	fileExport(deciphered, alphabet);

}

int caesarCryptanalysis(byte* cipher, int cipherSize, const double* theoricalDistrib, byte* alphabet, int alphabetSize){
	
	double* T = calloc(alphabetSize, sizeof(double));
	int mostProbableKey;
	double minimum;
	double* decipheredCharDistrib;
	
	byte* deciphered;
	
	for(int k = 0; k < alphabetSize; k++){
		
		T[k] = 0;
		
		deciphered = caesarDecrypt(cipher, cipherSize, alphabet, alphabetSize, alphabet[k]);
		decipheredCharDistrib = getCipherCharDistribution(deciphered, cipherSize, alphabet, alphabetSize, DISTRIBUTION_MODE_PROBABILITY);
		
		for(int j = 0; j < alphabetSize; j++)
			T[k] += powerOf(cipherSize*decipheredCharDistrib[j] - cipherSize*theoricalDistrib[j], 2) / max(cipherSize*theoricalDistrib[j], 1.0);
		
		
		if((k == 1) || ((T[k] < minimum) && (T[k] > 0.00))){
			mostProbableKey = k;
			minimum = T[k];
		}
		
	}
	
	return mostProbableKey;

}

void vigenereCryptanalysis(byte *cipher, int cipherSize,
						   double *theoricalDistrib,
						   byte* alphabet, int alphabetSize,
						   double threshold, int keySizeMax,
                           byte **key, int *keySize) {
	
	*keySize = friedmanTest(cipher, cipherSize, threshold, keySizeMax, alphabet, alphabetSize);
	
	byte** victims = getSubString(cipher, *keySize, getSubStringsSize(cipherSize, *keySize));
	(*key) = calloc(*keySize, sizeof(byte));
	
	for(int i = 0; i < *keySize; i++)
		(*key)[i] = alphabet[caesarCryptanalysis(victims[i], getSubStringsSize(cipherSize, *keySize), theoricalDistrib, alphabet, alphabetSize)];
		
	
}

/**
 *
 * @param cipher
 * @param threshold
 * @param keySizeMax
 * @param alphabet
 * @return
 */
int friedmanTest(byte* cipher, int cipherSize, double threshold, int keySizeMax, const byte* alphabet, int alphabetSize){

	double a, icnSum;
	byte** blocs;
	int subsNb;
	
	for(int n = 1; n <= keySizeMax; n++){
		
		subsNb = getSubStringsSize(cipherSize, n);
		blocs = getSubString(cipher, n, subsNb);
		
		icnSum = 0;
		for(int j = 0; j < n; j++)
			icnSum += getICN(blocs[j], subsNb, alphabet, alphabetSize);
		
		a = icnSum / (double)n;
		
		if(a >= threshold)
			return n;
	
	}
	
	return 0;

}

unsigned int getSubStringsSize(int cipherSize, int strSize){
	
	unsigned int subsNb = (int)(cipherSize / strSize);
	if((cipherSize % strSize) != 0)
		subsNb++;
	
	return subsNb;
}

byte** getSubString(const byte* text, int size, int subsNb){
	
	int forward = 0;
	byte** subStrings = calloc(subsNb, sizeof(byte*));
	
	for(int i = 0; i < subsNb; i++){
	
		subStrings[i] = calloc(size + 1, sizeof(byte));
		for(int j = 0; j < size; j++){
			
			subStrings[i][j] = text[forward];
			forward++;
			
		}
		
		subStrings[i][size] = '\0';
	}
	
	byte** blocs = calloc(size, sizeof(byte*));
	
	for(int i = 0; i < size; i++){
		
		blocs[i] = calloc(subsNb + 1, sizeof(byte));
		
		for(int j = 0; j < subsNb; j++){
			
			blocs[i][j] = subStrings[j][i];
			
		}
		
		blocs[i][subsNb] = '\0';
		
	}
	
	return blocs;
}

double getICN(const byte* text, int textSize, const byte* alphabet, int alphabetSize){
	
	double* frequencies = getCipherCharDistribution(text, textSize, alphabet, alphabetSize, DISTRIBUTION_MODE_INT);
	double ic = 0;
	
	if(textSize == 1)
		return 1 / (double)alphabetSize;
	
	for(int i = 0; i < alphabetSize; i++){
		ic += frequencies[i]*(frequencies[i] - 1);
	}
	
	
	return alphabetSize * (ic / (textSize*(textSize - 1)));

}

double* getCipherCharDistribution(const byte* cipher, const int cipherSize,
								  const byte* alphabet, const int alphabetSize,
								  int mode_probability){
	
	double* cipherDistrib = calloc(alphabetSize, sizeof(double));
	
	for(int i = 0; i < alphabetSize; i++){
		
		cipherDistrib[i] = 0;
		
		for(int j = 0; j < cipherSize; j++)
			cipherDistrib[i] += getKronecker(cipher[j], alphabet[i]);
		
		if(mode_probability == DISTRIBUTION_MODE_PROBABILITY)
			cipherDistrib[i] /= cipherSize;
		
	}
	
	return cipherDistrib;

}

int getKronecker(byte first, byte second){
	
	return (first == second) ? 1 : 0;
	
}

double powerOf(double value, int power){
	
	if(power == 0)
		return 1;
	
	for(int i = 1; i < power; i++)
		value *= value;
	
	return value;
	
}

double max(double first, double second){
	
	return (first >= second) ? first : second;
	
}

double abs_double(double x){
	
	return (x >= 0) ? x : -x;
	
}