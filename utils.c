//
// Created by Vanilor on 4/2/2020.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "consts.h"
#include "utils.h"
#include "utils_file.h"


/**
 * ****************************
 * GENERAL UTILS
 * ****************************
 */
 
void printHexPool(byte* text, int textSize, byte* title){

	printf("%s : ", title);
	for(int i = 0; i < textSize; i++)
		printf("%x ", text[i]);
	printf("\n");

}
 
void fileExport(byte* content, byte* alphabet){
	
	int export;
	
	do {
		printf("Voulez-vous exporter le texte déchiffré ? \n");
		scanf("%d", &export);
	}while((export != FILE_EXPORT) && (export != FILE_NOEXPORT));
	
	if(export == FILE_EXPORT){
		
		printf("Entrez un chemin et nom de fichier (Il sera créé si inexistant) : \n");
		byte* fileName = getString(alphabet, MODE_STRING_UNFILTERED);
		File* f = fopen(fileName, "wb");
		decode32(f, content, strlen(content));
		fclose(f);
	}
	
}
 
byte* handleInput(byte* alphabet){
	
	int inputMode = INPUT_MODE_TYPED;
	byte* input;
	int size;
	
	do {
		
		printf("Souhaitez-vous taper un message ou utiliser un fichier ? (0 pour taper, 1 pour un fichier) \n");
		scanf("%d", &inputMode);
		emptyBuffer();
		
	} while ((inputMode != INPUT_MODE_TYPED) && (inputMode != INPUT_MODE_FILE));
	
	if(inputMode == INPUT_MODE_FILE) {
		
		printf("Indiquez le chemin du fichier relatif à l'exécutable : \n");
		byte* messagePath = getString(alphabet, MODE_STRING_UNFILTERED);
		
		FILE* f = fopen(messagePath, "rb");
		if(!f){
			printf("[ERREUR] Le fichier n'a pas pu être ouvert, essayez-en un autre. \n");
			return NULL;
		}
		
		// We can use another way to do this without allocMessage and avoid to pass size and input parameters
		// by using a realloc dynamically as we run through the file
		allocMessage(f, &input, &size);
		encode32(f, input, size);
		fclose(f);
		
	}
	
	//Use typed message as default input
	else {
		
		printf("Message à convertir :\n");
		input = getString(alphabet, MODE_STRING_FILTERED);
	}
	
	return input;

}

short checkProbabilities(const double* array, int alphabetSize){
	
	double total = 0;
	for(int i = 0; i < alphabetSize; i++)
		total += array[i];
	
	if((total == 100.00) || (total == 1.00))
		return 1;
	
	return 0;
	
}

void emptyBuffer()
{
	int c = 0;
	while (c != '\n' && c != EOF)
	{
		c = getchar();
	}
}


/**
 *
 * @param c
 * @param alphabet
 * @return
 */
short isInAlphabet(byte c, byte* alphabet, int alphabetSize) {
	
	for (int i = 0; i < alphabetSize; i++)
		if (c == alphabet[i])
			return 1;
		
	return 0;
	
}

/**
 *
 * @param alphabet
 * @param mode
 * @return byte*
 */
byte* getString(byte* alphabet, int filtered) {
	
	byte* input = calloc(1, sizeof(byte));
	
	int c = EOF;
	unsigned int i = 0;
	
	//Accept user input until hit enter or end of file
	while ((c = getchar()) != '\n' && c != EOF)
	{
		
		if (!isInAlphabet(c, alphabet, strlen(alphabet)) && (filtered == MODE_STRING_FILTERED)) {
			
			printf("Le caractère %c n'est pas compris dans l'alphabet, il ne sera pas traité dans le chiffrement. \n", c);
			continue;
			
		}
		
		input[i++] = c;
		
		//Realloc input to + 1 to get the next user input char
		input = realloc(input, i + 1);
	}
	
	input[i] = '\0';
	return input;
	
}

/**
 *
 * @param value
 * @param m
 * @return unsigned int
 *
 */
unsigned int modulo_u(int value, unsigned int m) {
	
	int mod = value % (int) m;
	if (value < 0)
		mod += m;
	
	return mod;
}

/**
 *
 * @param needle
 * @param haystack
 * @return
 */
int array_search(byte needle, byte* haystack, int haystackSize){
	
	for(int i = 0; i < haystackSize; i++)
		if(haystack[i] == needle)
			return i;
	
	return -1;
}


/**
 *
 * Extended ASCII ISO norm for character encoding : UTF-8 STRICTLY
 * Please refer to https://www.utf8-chartable.de/
 *
 * @param c
 * @return
 */
byte convertExtendedASCII(byte c){
	
	//Convert any lowercase letter to uppercase
	if((c >= 0x61) && (c <= 0x7A))
	{
		c -= 32;
	}
		
	/*
	 * Convert accentuated characters into corresponding uppercase
	 */
	
	//A-like characters
	else if(((c >= 0xC0) && (c <= 0xC5)) || ((c >= 0xE0) && (c <= 0xE5)))
	{
		c = 'A';
	}
		
	//E-like characters + æ / Æ (ae but strange, lower/uppercase)
	else if(((c >= 0xC8) && (c <= 0xCB)) || ((c >= 0xE8) && (c <= 0xEB)) || (c == 0xC6) || (c == 0xE6))
	{
		c = 'E';
	}
		
	//I-like characters
	else if(((c >= 0xCC) && (c <= 0xCF)) || ((c >= 0xEC) && (c <= 0xEF)))
	{
		c = 'I';
	}
		
	//O-like characters
	else if(((c >= 0xD2) && (c <= 0xD6)) || ((c >= 0xF2) && (c <= 0xF6)))
	{
		c = 'O';
	}
		
	//U-like characters
	else if(((c >= 0xD9) && (c <= 0xDC)) || ((c >= 0xF9) && (c <= 0xFC)))
	{
		c = 'U';
	}
		
	//Y-like characters, but avoid
	else if((c == 0xDD) || (c == 0xFD) || (c == 0xFF))
	{
		c = 'Y';
	}
		
	//N-like characters
	else if((c == 0xD1) || (c == 0xF1))
	{
		c = 'N';
	}
		
	//C-like characters
	else if((c == 0xC7) || (c == 0xE7))
	{
		c = 'C';
	}
	
	//In case there's no corresponding accentuated or lowercase char, returning char
	return c;
}