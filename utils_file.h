//
// Created by Vanilor on 4/3/2020.
//

#pragma once

static byte bits = 0;
static byte octet = 0;
static int offset = 0;
static int mask = 0;

int allocMessage(FILE *f, byte **message, int *size);
void encode32(FILE *file, byte *message, int size);
void decode32(File* f, byte* content, int size);