//
// Created by Vanilor on 4/3/2020.
//

#pragma once


/*
 * ===========================
 * CRYPTANALYSIS MAIN FUNCTIONS
 * ===========================
 */
int caesarCryptanalysis(byte* cipher, int cipherSize, const double* theoricalDistrib, byte* alphabet, int alphabetSize);
void vigenereCryptanalysis(byte *cipher, int cipherSize,
                           double *theoricalDistrib,
                           byte* alphabet, int alphabetSize,
                           double threshold, int keySizeMax,
                           byte **key, int *keySize);
int friedmanTest(byte* cipher, int cipherSize, double threshold, int keySizeMax, const byte* alphabet, int alphabetSize);

/*
 * ============================
 * UTILS FUNCTIONS
 * ============================
 */


byte** getSubString(const byte* text, int size, int subsNb);
unsigned int getSubStringsSize(int cipherSize, int strSize);
double getICN(const byte* text, int textSize, const byte* alphabet, int alphabetSize);
void cryptanalysisMode(byte* alphabet, double* theoricalDistrib, double langageICN, unsigned int algorithm);
double* getCipherCharDistribution(const byte* cipher, const int cipherSize,
								  const byte* alphabet, const int alphabetSize,
								  int mode_probability);
int getKronecker(byte first, byte second);
double powerOf(double value, int power);
double max(double first, double second);
double abs_double(double x);