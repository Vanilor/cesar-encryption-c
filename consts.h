#pragma once

#include <stdlib.h>
#include <stdio.h>

typedef FILE File;
typedef unsigned char byte;

enum {
	MODE_DECRYPT                    = 0,
	MODE_ENCRYPT                    = 1,
	MODE_STRING_UNFILTERED          = 0,
	MODE_STRING_FILTERED            = 1,
	INPUT_MODE_TYPED                = 0,
	INPUT_MODE_FILE                 = 1,
	ALGORITHM_CAESAR                = 0,
	ALGORITHM_VIGENERE              = 1,
	MODE_ENCRYPTION                 = 0,
	DISTRIBUTION_MODE_PROBABILITY   = 0,
	DISTRIBUTION_MODE_INT           = 1,
	FILE_EXPORT                     = 1,
	FILE_NOEXPORT                   = 0,
};