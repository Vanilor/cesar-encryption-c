#include <stdio.h>
#include <string.h>

#include "consts.h"
#include "utils.h"
#include "utils_file.h"
#include "encryption.h"
#include "cryptanalysis.h"

#define CHALLENGE2 1

int main(int argc, char *argv[]) {
	
	if(CHALLENGE2){
		
		const int alphabetSize = 256;
		const int cryptanalysisSize = 687;
		const int threshold = 10;
		
		byte* alphabet = calloc(alphabetSize, sizeof(byte));
		double* theoricalDistrib = calloc(alphabetSize, sizeof(double));
		
		//Loading full UTF-8 alphabet
		//Please refer to https://www.utf8-chartable.de/
		for(int i = 0; i < alphabetSize; i++){
			alphabet[i] = i;
			theoricalDistrib[i] = 0;
		}
		theoricalDistrib[0] = 1.0;
		
		File* f = fopen("../challenge_2", "rb");
		if(!f){
			printf("Cannot open challenge2 input file for reading \n");
			return 0;
		}
		byte* cipher = calloc(cryptanalysisSize + 1, sizeof(byte)); //Approximately first 0.25% of the file
		byte nextByte;
		
		unsigned int i = 0;
		while (i < cryptanalysisSize) {
			
			fread(&nextByte, sizeof(byte), 1, f);
			cipher[i++] = nextByte;
		}
		
		//Add null-terminating character
		cipher[cryptanalysisSize] = '\0';
		fclose(f);
		free(f);
		f = NULL;
		
		byte* key;
		int keySize;
		vigenereCryptanalysis(cipher, cryptanalysisSize, theoricalDistrib, alphabet, alphabetSize, threshold, cryptanalysisSize, &key, &keySize);
		
		File* test = fopen("../challenge_2", "rb");
		byte* imgCiphered;
		int imgSize;
		
		allocMessage(test, &imgCiphered, &imgSize);
		i = 0;
		while (i < imgSize) {
			fread(&nextByte, sizeof(byte), 1, test);
			imgCiphered[i++] = nextByte;
		}
		byte* imgDeciphered = vigenereDecrypt(imgCiphered, imgSize, alphabet, alphabetSize, key, keySize);
		fclose(test);
		free(test);
		test = NULL;
		
		
		File* output = fopen("../challenge2_output.jpg", "wb");
		if(!output){
			printf("Cannot open output for writing \n");
			return 0;
		}
		printHexPool(imgDeciphered, imgSize, "IMG");
		i = 0;
		while(i < imgSize)
			fwrite(&(imgDeciphered[i++]), sizeof(byte), 1, output);
		
		
		fclose(output);
		free(output);
		output = NULL;
		
		free(cipher);
		free(alphabet);
		free(theoricalDistrib);
		free(imgDeciphered);
		free(imgCiphered);
		cipher = NULL;
		alphabet = NULL;
		theoricalDistrib = NULL;
		imgDeciphered = NULL;
		imgCiphered = NULL;
		
		return 1;
		
	}
	
	
	//@TODO Add Load alphabet from file feature
	byte *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ .,?!'";
	//We are using double instead of double, because double implies calculation imprecision.
	const double theoricalDistrib[] = {
			0.0756, 0.0082, 0.0211, 0.0272, 0.1274, 0.0076,
			0.0083, 0.0061, 0.0675, 0.0071, 0.0000, 0.0429,
			0.0260, 0.0573, 0.0412, 0.0230, 0.0085, 0.0519,
			0.0634, 0.0583, 0.0500, 0.0173, 0.0000, 0.0024,
			0.0019, 0.0026, 0.1677, 0.0119, 0.0062, 0.0008,
			0.0020, 0.0086
	};
	//Source : https://fr.wikipedia.org/wiki/Indice_de_co%C3%AFncidence#Autres_langues
	const double frenchICN = 2.0;
	
	
	
	if (!checkProbabilities(theoricalDistrib, (int) strlen(alphabet))) {
		printf("[ERREUR] Le tableau des probabilités d'apparition théorique n'est pas valide, vérifiez-le.");
		return 0;
	}
	
	
	printf("[INFO] Alphabet en cours d'utilisation : %s \n", alphabet);
	printf("[INFO] Le tableau des probabilités est valide. \n\n");
	
	unsigned int algorithm = ALGORITHM_CAESAR;
	int again = 1, mode = MODE_ENCRYPTION;
	
	do {
		//@TODO Don't ask this question and just determine by the lenght of the key
		printf("Quel algorithme souhaitez-vous utiliser ? 0 pour César, 1 pour Vigenere \n");
		scanf("%d", &algorithm);
		emptyBuffer();
		
	} while ((algorithm != ALGORITHM_CAESAR) && (algorithm != ALGORITHM_VIGENERE));
	
	while (again == 1) {
		
		printf("Souhaitez-vous utiliser le mode Chiffrement-Déchiffrement ( 0 ) ou le mode Cryptanalyse ( 1 ) ? \n");
		scanf("%d", &mode);
		
		if (mode == MODE_ENCRYPTION)
			encryptionMode(alphabet, algorithm);
		else
			cryptanalysisMode(alphabet, theoricalDistrib, frenchICN, algorithm);
		
		printf("\n\nContinuer ?\n");
		scanf("%d", &again);
		emptyBuffer();
		
		if (again == 1)
			printf("\n\n\n");
		
	}
	
	return 1;
	
}